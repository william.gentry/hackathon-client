import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from './App';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  brand: {
    color: 'orangered',
    fontSize: '1.3em'
  },
  bar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  spacer: {
    flex: '1 1 auto'
  },
  navLink: {
    fontSize: '1.25em',
    marginLeft: '5px',
    marginRight: '5px'
  }
});

const Nav = props => {
  const [state] = useContext(AppContext);
  const { email, loggedIn } = state;
  const styles = useStyles();

  if (email && loggedIn) {
    return (
      <div className={styles.bar}>
        <Link to='/home' className={styles.brand}>
          Snippit
        </Link>
        <span className={styles.spacer}></span>
        <Link to='/home' className={styles.navLink}>
          Home
        </Link>
        <Link to='/editor' className={styles.navLink}>
          New Snippit
        </Link>
        <span className={styles.spacer}></span>
        <Link to='/login'>Logout</Link>
      </div>
    );
  } else {
    return (
      <div className={styles.bar}>
        <Link to='/home' className={styles.brand}>
          Snippit
        </Link>
        <span className={styles.spacer}></span>
        <Link to='/login'>Login</Link>
      </div>
    );
  }
};

export default Nav;
