import React, {Component} from "react";
import Comment from '../comment/Comment';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Badge from '@material-ui/core/Badge';
import { FaThumbsUp } from 'react-icons/fa';
import Accordion from 'react-bootstrap/Accordion';
import Paper from '@material-ui/core/Paper';

class Snippet extends Component {

    constructor(props) {
      super(props);
      this.tagElements = [];
      this.commentElements = [];
      this.createTags(this.props.tags);
      this.createComments(this.props.comments);
    }

    createTags = function(tags) {
      console.log(tags)
      for (const [index, value] of tags.entries()) {
        this.tagElements.push(<Badge variant="standard" key={value["id"]}>{value["name"]}</Badge>)
      }
    }

    createComments = function(comments) {
        if (comments) {
            for (const [index, value] of comments.entries()) {
                this.commentElements.push(<Comment key={value["id"]} content={value["content"]} author={value["author"]}/>)
            }
        }
    }

    render () {
      return (
        <Card className="card">
          <CardContent>
              <h1>{this.title}</h1>
              <div>
                  <Paper style={{padding:'5px'}}>{this.props.content}</Paper>
                </div>
                <div>
                  {this.tagElements} &nbsp;
                  <FaThumbsUp /> {this.props.likes}
                </div>
                <div>
                    {this.commentElements}
                </div>
          </CardContent>
      </Card>
      )  
    }
  
  }

  export default Snippet;




  
