import { useState, useEffect } from 'react';
import {ALL_EMAILS, CORS_URL, DEFAULT_HEADERS, TEST_EMAILS} from "../constants";

const getAllEmails = () => {
    return fetch(ALL_EMAILS, DEFAULT_HEADERS)
};

export const useEmails = () => {
    const [allEmails, setAllEmails] = useState([]);

    useEffect(() => {
        // setAllEmails(TEST_EMAILS);
        (async () => {
            const response = await getAllEmails();
            if (response.ok) {
                const data = await response.json();
                if (data && data.length) {
                    setAllEmails(data);
                }
            }
        })()
    }, []);


    return allEmails;
};
