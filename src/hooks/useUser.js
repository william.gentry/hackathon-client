import { useState, useEffect } from 'react';
import {DEFAULT_HEADERS} from "../constants";

const getUserByEmail = email => {
    return fetch(`http://localhost:10002/users/email/${email}`, {
        mode: "cors",
        headers: {
            "Access-Control-Allow-Origin": "http://localhost:10002",
            "Access-Control-Allow-Header": "Access-Control-Allow-Origin"
        }
    });
};

const useUser = email => {
    const [user, setUser] = useState({});
    useEffect(() => {
        (async () => {
            const response = await getUserByEmail(email.replace("@", "%40"));
            if (response.ok) {
                const json = response.json();
                if (json && Object.keys(json) && Object.keys(json).length) {
                    setUser(json);
                }
            }
        })();
    }, [email]);
    return user;
};

export default useUser;
