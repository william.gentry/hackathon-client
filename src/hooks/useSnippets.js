import {useContext, useEffect, useState} from 'react';
import {ALL_SNIPPETS, DEFAULT_HEADERS, TEST_SNIPPETS} from "../constants";
import {AppContext} from "../App";

const getAllSnippets = () => {
    return fetch(ALL_SNIPPETS, DEFAULT_HEADERS);
};

export const useSnippets = email => {
    const [state, dispatch] = useContext(AppContext);
    const [snippets, setSnippets] = useState([]);
    useEffect(() => {
        dispatch({ type: "FETCHED_ALL_SNIPPETS", snippets: TEST_SNIPPETS });
        // setSnippets(TEST_SNIPPETS);
        (async () => {
            const response = await getAllSnippets();
            if (response.ok) {
                const json = await response.json();
                if (json && json.length) {
                    setSnippets(json);
                    const snippets = json.filter(snippet => snippet.author.email === email);
                    dispatch({ type: "FETCHED_ALL_SNIPPETS", snippets });
                }
            }
        })();
    }, [email]);
    return snippets;
};
