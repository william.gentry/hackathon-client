export const CORS_URL = "http://localhost:3000";
export const BASE_URL = "http://localhost:10001";
export const USERS_URL = 'http://localhost:10002';
export const ALL_EMAILS = `${USERS_URL}/users`;
export const ALL_SNIPPETS = `${BASE_URL}/snippets`;
export const GET_SNIPPET_BY_ID = id => {
    return `${ALL_SNIPPETS}/${id}`;
};

export const DEFAULT_HEADERS = {
    mode: "cors",
    headers: {
        "Accept": "application/json",
        "Access-Control-Allow-Origin": CORS_URL
    }
};

export const POST_HEADERS = {
    mode: "cors",
    method: "POST",
    headers: {
        "Accept": "application/json",
        "Access-Control-Allow-Origin": CORS_URL
    }
};

export const TEST_EMAILS = [
    { value: "william@test.com"},
    { value: "august@test.com"},
    { value: "julie@test.com"},
    { value: "ruiz@test.com"},
    { value: "oberlies@test.com"},
    { value: "jonathon@test.com"},
];

export const TEST_SNIPPETS = [
    {
        "id": {},
        "content": "Here is my fifth snippet",
        "tags": [
            {"id":	{}, "name": "Does this have an id also?"}, {"id": {}, "name": "Or this one also maybe?"}],
        "likes": 5,
        "author": {
            "id": "5de8faae39aa8351d51d3f5e",
            "firstName": "Benjamin",
            "lastName": "Smith",
            "email": "benjamin.smith@gmail.com"
        },
        "comments": [{"id": {}, "content": "This is my first comment", "author": {
                "id": "5de8faae39aa8351d51d3f5e",
                "firstName": "Benjamin",
                "lastName": "Smith",
                "email": "benjamin.smith@gmail.com"
            }}]
    }
];
