import React, {useContext} from "react";
import {AppContext} from "../App";
import { Redirect } from 'react-router-dom';

const IsLoggedIn = props => {
    const [state, dispatch] = useContext(AppContext);
    if (state.loggedIn) {
        return props.children
    } else {
        return <Redirect to={"/login"} />
    }
};

export default IsLoggedIn;
