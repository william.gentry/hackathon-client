import React, { createContext, useContext, useReducer } from 'react';

export const AppContext = createContext({ email: ""});

export const ContextProvider = (({ reducer, initialState, children }) => {
   return( <AppContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </AppContext.Provider>);
});

export const useAppState = () => useContext(AppContext);
