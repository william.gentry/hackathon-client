import Snippet from './snippet/Snippet';
import React, { useReducer } from 'react';
import './App.css';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles';
import { revatureTheme } from './Theme';
import LoginCard from './login/LoginCard';
import Home from './Home/Home';
import IsLoggedIn from './hoc/isLoggedIn';
import Nav from './Nav';
import Editor from './Editor/Editor';

const initialState = {
  email: '',
  loggedIn: false,
  snippets: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SELECT_USER':
      return { ...state, email: action.email, loggedIn: true };
    case 'FETCHED_ALL_SNIPPETS':
      return { ...state, snippets: action.snippets };
    case 'SUBMIT_SNIPPET':
      return { ...state, snippets: [...state.snippets, action.snippet] };
    default:
      return state;
  }
};

const RedirectToLogin = props => {
  const { currentUser } = props;

  return <Redirect to={currentUser ? '/home' : '/login'} />;
};

export const AppContext = React.createContext({});

export const ContextProvider = AppContext.Provider;
export const ContextConsumer = AppContext.Consumer;

const App = routerProps => {
  const useAppState = useReducer(reducer, initialState);
  return (
    <BrowserRouter>
      <ThemeProvider theme={revatureTheme}>
        <ContextProvider value={useAppState}>
          <Nav />
          <Switch>
            <Route
              exact
              path='/login'
              render={routerProps => <LoginCard {...routerProps} />}
            ></Route>
            <Route
              exact
              path='/home'
              render={routerProps => <Home {...routerProps} />}
            ></Route>
            <Route path='/editor' component={Editor} />
            <Route component={RedirectToLogin} />
          </Switch>
        </ContextProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default App;
