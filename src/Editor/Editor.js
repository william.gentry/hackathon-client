import React, { useContext, useState } from 'react';
import { AppContext } from '../App';
import { makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import {ALL_SNIPPETS, CORS_URL, POST_HEADERS} from "../constants";
import useUser from "../hooks/useUser";

const useStyles = makeStyles({
  error: {
    display: 'flexbox',
    border: '1px solid red',
    backgroundColor: 'rgba(255, 0, 0, 0.3)'
  },

  errorContent: {
    display: 'flex',
    color: 'red',
    justifyContent: 'center',
    alignItems: 'center'
  },
  page: {
    display: 'flex',
    flexDirection: 'column',
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center'
  },

  textEditor: {
    color: 'black',
    border: 'inset 1px solid black'
  }
});
const Editor = props => {
  const [value, setValue] = useState('');
  const [state, dispatch] = useContext(AppContext);
  const { email, loggedIn } = state;
  const styles = useStyles();
  const editor = (
    <textarea
      rows='33'
      cols='100'
      className={styles.textEditor}
      value={value}
      onChange={e => setValue(e.target.value)}
    ></textarea>
  );
  const clearClick = () => {
    setValue('');
  };
  const submitClick = () => {
    const snippet = {
      id: {},
      author: email,
      content: value,
      tags: [],
      comments: []
    };
    (async () => {
      const response = await fetch(ALL_SNIPPETS, {
        mode: "cors",
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": CORS_URL
        },
        body: JSON.stringify(snippet)
      });
      if (response.ok) {
        const json = await response.json();
        // dispatch({ type: 'SUBMIT_SNIPPET', snippet });
        if (json) {
          console.log(json);
        }
      }
    })();
    setValue('');
  };

  if (email && loggedIn) {
    return (
      <div className={styles.page}>
        {editor}
        <div>
          <Button variant='contained' color='primary' onClick={submitClick}>
            Submit
          </Button>
          <Button variant='contained' color='secondary' onClick={clearClick}>
            Clear
          </Button>
        </div>
      </div>
    );
  } else {
    return (
      <div className={styles.error}>
        <div className={styles.errorContent}>
          Please return to&nbsp; <Link to='/login'>Login</Link>
        </div>
      </div>
    );
  }
};

export default Editor;
