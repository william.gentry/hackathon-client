import React, {useContext} from "react";
import {Card, CardContent, CardHeader, Container, makeStyles} from "@material-ui/core";
import Login from "./Login";
import {AppContext} from "../App";

export const SELECT_USER_ACTION = email => {
    return {
        type: "SELECT_USER",
        email
    }
};

const useStyles = makeStyles({
    card: {
        marginTop: "5%"
    }
});

const LoginCard = props => {
    const styles = useStyles();
    return (
        <Container maxWidth={'md'} className={styles.card}>
            <Card>
                <CardHeader title={"Select User"} />
                <CardContent>
                    <Login {...props} />
                </CardContent>
            </Card>
        </Container>
    )
};

export default LoginCard;
