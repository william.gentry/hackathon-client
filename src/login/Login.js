import React, {useContext} from 'react';
import EmailSelector from "./EmailSelector";
import { Button } from "@material-ui/core";
import {AppContext} from "../App";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    loginCard: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '80%'
    }
});

const Login = props => {
    const [state, dispatch] = useContext(AppContext);
    const { email } = state;
    const styles = useStyles();
    function handleEmailChange(event) {
        event.preventDefault();
        dispatch({ type: "SELECT_USER", email: event.target.value });
    };

    function handleSignIn() {
        props.history.push('/home');
    }
    if (email !== "") {
        return (
            <div className={styles.loginCard}>
                <EmailSelector handleEmailChange={handleEmailChange} selectedEmail={email} />
                <Button variant={"contained"} color="primary" size={"small"} onClick={() => handleSignIn()}>Continue</Button>
            </div>
        )
    } else {
        return (
            <EmailSelector handleEmailChange={handleEmailChange} selectedEmail={email} />
        )
    }
};

export default Login;
