import React, {useContext, useState} from 'react';
import {useEmails} from "../hooks/useEmails";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import {AppContext} from "../App";


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));


const EmailSelector = props => {
    const [state, dispatch] = useContext(AppContext);
    const { email } = state;
    const styles = useStyles();
    const emails = useEmails();

    const handleEmailChange = event => {
        dispatch({type: "SELECT_USER", email: event.target.value})
    };

    if (emails && emails.length) {
        return (
            <div>
                <FormControl className={styles.formControl}>
                    <InputLabel id={"email-selector-id"}>Emails</InputLabel>
                    <Select
                        labelId={"email-select-label"}
                        id={"email-select"}
                        value={email}
                        onChange={e => handleEmailChange(e)}
                    >
                        {emails.map(user => {
                            return <MenuItem value={user} key={`email-${user.email}`} >{user.email}</MenuItem>
                        })}
                    </Select>
                </FormControl>
            </div>
        )
    } else {
        return (
            <div>
                <FormControl>
                    <InputLabel id={"email-selector-id"}>Emails</InputLabel>
                    <Select
                        labelId={"email-select-label"}
                        id={"email-select"}
                        value={email}
                    >
                        <MenuItem value={"N/A"}>No Emails Found</MenuItem>
                    </Select>
                </FormControl>
            </div>
        )
    }
};

export default EmailSelector;
