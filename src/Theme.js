import {createMuiTheme} from "@material-ui/core/styles";
import red from '@material-ui/core/colors/red';

export const revatureTheme = createMuiTheme({
    palette: {
        type: "dark",
        primary: {
            main: "#F26925"
        },
        secondary: {
            main: "#474C55"
        },
        error: {
            main: red[500]
        },
    }
});
