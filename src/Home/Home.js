import React, {useContext} from "react";
import {AppContext} from "../App";
import {useSnippets} from "../hooks/useSnippets";
import Snippet from "../snippet/Snippet";


const Home = props => {
    const [state, dispatch] = useContext(AppContext);
    const snippets = useSnippets(state.email);
    return (
        <div>
            <h1>Home works!!!</h1>
            { snippets.map(snippet => {
                return <Snippet tags={snippet.tags} id={snippet.id} author={snippet.author} comments={snippet.comments} likes={snippet.likes} content={snippet.content}/>
            })}
        </div>
    )
};

export default Home;
